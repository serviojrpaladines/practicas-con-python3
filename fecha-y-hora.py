import datetime

def ver_instrucciones():
  print("Estas son las opciones que puedes realizar:")
  print("1 Ver la hora")
  print("2 Ver la fecha y hora")
  print("3 Ver la Central")
  print("4 Ver la hora en San Fransico-USA")
  print("5 Ver las instrucciones nuevamente")
  print("6 Salir")

def ver_hora(zona):
  formato = "%H:%M:%S"
  zona_horaria = datetime.timezone(datetime.timedelta(hours=zona))
  hora_actual = datetime.datetime.now(zona_horaria).time()
  hora_formateada = hora_actual.strftime(formato)
  return hora_formateada

def ver_fecha_hora():
  formato = "%B %d, %Y  %H:%M:%S"
  zona_horaria = datetime.timezone(datetime.timedelta(hours=-5))
  fecha_actual = datetime.datetime.now(zona_horaria)
  fecha_formateada = fecha_actual.strftime(formato)
  print("La fecha y hora actual es: {}".format(fecha_formateada))

def ver_reloj():
  print("Bienvenid@ al Reloj del Mundo")
  ver_instrucciones()
  while True:
    operacion = input(": ")
    if operacion == "1":
      print("La hora exacta es: {}".format(ver_hora(-5)))
    elif operacion == "2":
      ver_fecha_hora()
    elif operacion == "3":
      print("La hora Central es: {}".format(ver_hora(-6)))
    elif operacion == "4":
      print("La hora San Francisco-USA es: {}".format(ver_hora(-8)))
    elif operacion == "5":
      ver_instrucciones()
    elif operacion == "6":
      break
    else:
      print("No reconozco esa opcion")

ver_reloj()