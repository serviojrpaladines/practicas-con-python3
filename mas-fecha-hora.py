import datetime

print(datetime.datetime.now().day)
print()

#Trabajar con variables individuales de fecha y hora actual
now = datetime.datetime.now()

print("anio", now.year)

print("mes", now.month)

print("dia", now.day)

print("hora", now.hour)

print("minuto", now.minute)

print("segundos", now.second)

print("micro-segundos", now.microsecond)

#Reemplazar variables de fecha y hora actual

now = now.replace(minute=0, second=0)
print("minutos", now.minute)
print("segundos", now.second)
print("ahora", now)

#Trabajando con timedelta para sumar o restar fecha y tiempo
print(now + datetime.timedelta(days=5,minutes=3,hours=30))

#Podemos usar tambien las siguientes formas
print(now - datetime.timedelta(days=5))
print(now + datetime.timedelta(days=-5))

#Mostrar solo la fecha y solo hora
print(now.date())
print(now.time())

#Método COMBINE
today = datetime.datetime.today()
fecha_hoy = datetime.datetime.combine(datetime.datetime.today(),datetime.time())

print(fecha_hoy)

#Formateando la FECHA y HORA: usos de STRFTIME
#Página de referencia: http://strftime.org/

print(fecha_hoy.strftime("%B %d, %Y %H:%M"))

#Conviertiendo una CADENA en FECHA/HORA

fecha_cadena = "06/02/2017"
fecha_formato = datetime.datetime.strptime(fecha_cadena,"%d/%m/%Y")
print(fecha_formato, type(fecha_formato))